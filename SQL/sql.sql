CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user (id int SERIAL,login_id VARCHAR(256) NOT NULL , name varchar(256) NOT NULL,birth_date DATE NOT NULL,password VARCHAR(256) NOT NULL,create_date DATETIME NOT NULL,update_date DATETIME NOT NULL);