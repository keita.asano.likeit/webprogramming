<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ユーザ新規登録画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- オリジナルCSS読み込み -->
  <link href="css/original/common.css" rel="stylesheet">

</head>

<body>

  <!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.html">ユーザ管理システム</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="nav-link" href="#">${userInfo.name}さん</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>
  <!-- /header -->

  <!-- body -->
  <div class="container">


    <!-- エラーメッセージのサンプル(エラーがある場合のみ表示) -->
    <c:if test="${errMsg != null}" >
    <div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

    <form method="post" action="UserCreateServlet" class="form-horizontal">
      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="loginId" name="loginId" placeholder="ログインIDを入力してください" value="${loginId}">
        </div>
      </div>

      <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="password" name="password"placeholder="パスワードを入力してください"value="${password}">
        </div>
      </div>

      <div class="form-group row">
        <label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="passwordConf"name="rePassword"placeholder="再度入力してください"value="${rePassword}">
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="userName"name="userName"placeholder="ユーザー名を入力してください"value="${userName}">
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <input type="date" class="form-control" id="birthDate" name="birthDate"value="${birhtDate}">
        </div>
      </div>

      <div class="submit-button-area">

        <center><button type="submit" value="検索" class="col-sm-2 btn btn-primary btn-lg btn-block　">登録</button></center>
      </div>

      <div class="col-xs-4">
        <a href="UserListServlet">戻る</a>
      </div>

    </form>


  </div>
  </div>





</body>

</html>