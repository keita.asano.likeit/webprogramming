<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/common.css" rel="stylesheet">
</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active">

			</ul>

				<li class="nav-item"></li>
				<li class="navbar-text">${userInfo.name}さん</li>
				<li class="dropdown"></li>
				<li class="nav-item"><a class="btn btn-primary"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->
	<!-- body -->

	<div class="container">

		<!-- 新規登録ボタン -->
		<div class="create-button-area">
			<a class="btn btn-outline-primary btn-lg" href="UserCreateServlet">新規登録</a>
		</div>


		<!-- 検索ボックス -->
		<div class="search-form-area">
			<div class="panel-body">
				<form method="post" action="UserListServlet" class="form-horizontal">
					<div class="form-group row">
						<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name = "loginId" id="loginId">
						</div>
					</div>

					<div class="form-group row">
						<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name= "userName"id=userName">
						</div>
					</div>

					<div class="form-group row">

						<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>

						<div class="row col-sm-10">
							<div class="col-sm-5">
								<input type="date" name="date-start" id="date-start"
									class="form-control" />
							</div>

							<div class="col-sm-1 text-center">~</div>
							<div class="col-sm-5">
								<input type="date" name="date-end" id="date-end"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="text-right">
						<button type="submit" value="検索"
							class="btn btn-primary form-submit">検索</button>
					</div>
				</form>
			</div>
		</div>

		<!-- 検索結果一覧 -->
		<div class="table-responsive">
			<table class="table table-striped">
				<thead class="thead-dark">
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<td>
								<c:if test="${userInfo.loginId == 'admin'}" >
									 <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
									<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
									<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
								</c:if>
								<c:if test="${userInfo.loginId != 'admin'}" >
									<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
									<c:if test="${userInfo.loginId == user.loginId }" >
									<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
									</c:if>
								</c:if></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	</div>

</body>

</html>