package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserCreateServlet
 */
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得


		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		if(!(password.equals(rePassword)))  {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "同じパスワードを入力してください");

			request.setAttribute("loginId", loginId);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDate", birthDate);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;

		}else if(loginId.equals("")||password.equals("")||rePassword.equals("")
				||userName.equals("")||birthDate.equals("")) {
			request.setAttribute("errMsg", "全て入力してください");

			request.setAttribute("loginId", loginId);
			request.setAttribute("password", password);
			request.setAttribute("rePassword", rePassword);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDate", birthDate);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDao userDao = new UserDao();
		String pass = userDao.Angou(password);
		String id = userDao.findById(loginId);
		if(id != null) {
			request.setAttribute("errMsg", "IDが重複してます");

			request.setAttribute("password", password);
			request.setAttribute("rePassword", rePassword);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDate", birthDate);


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;

		}


		userDao.insertUser(loginId, pass, userName,birthDate);
		// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");


	}

}
