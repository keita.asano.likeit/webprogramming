package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.userCheckUpdate(id);

		request.setAttribute("id", id);
		request.setAttribute("user", user);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();
		String pass = userDao.Angou(password);


		if(!(password.equals(rePassword)))  {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "同じパスワードを入力してください");

			request.setAttribute("id", id);
			request.setAttribute("loginId", loginId);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDate", birthDate);


			User user = userDao.userCheckUpdate(id);

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}else if(loginId.equals("")||userName.equals("")||birthDate.equals("")) {
			request.setAttribute("errMsg", "パスワード以外は必ず入力してください");

			request.setAttribute("loginId", loginId);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDate", birthDate);


			User user = userDao.userCheckUpdate(id);

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(password.equals("")&&rePassword.equals("")) {

			request.setAttribute("loginId", loginId);
			request.setAttribute("userName", userName);
			request.setAttribute("birthDate", birthDate);

			userDao.UpdateOthers(loginId, userName,birthDate);
			response.sendRedirect("UserListServlet");
			return;
		}



		userDao.userUpdate(loginId, userName,  birthDate, pass );
		// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");

	}

}
